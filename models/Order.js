const mongoose = require('mongoose')

const order_schema = new mongoose.Schema({
	userId:{
		type:String,
		required:[true, "Name is required"]
	},
	products:[
		{
			productId:{
				type:String,
				required:[true, "Product Id is required"]
			},
			quantity:{
				type:String,
				required:[true, "Quantity is required"]
			}
		}
	],
	totalAmount:{
		type:Number,
		required:[true, "Total Amount is required"]
	},
	purchasedOn: {
		type:Date,
		default: new Date()
	}
})

module.exports = mongoose.model('Order', order_schema)