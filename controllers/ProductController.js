const Product = require('../models/Product')


//add new product controller

module.exports.addProduct = (data) => {
	if(data.isAdmin){
		let new_product = new Product({
		name:data.product.name,
		description:data.product.description,
		price:data.product.price
	})
		

	return new_product.save().then((new_product, error) => {
		if(error){
			return false
		}

		return {
			message: 'New product successfully created!'
		}
	})
	}	


  let message = Promise.resolve({
  		message: 'User must be ADMIN to access this'
  })

	return message.then((value) => {
			return value
	})

}                                                                         

//get all active product     

module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then((result) => {
		return result
	})
}


//get single product controller

module.exports.getProduct = (product_id) => {
	return Product.findById(product_id).then((result) => {
		return result
	})
}


//update single product controller
module.exports.updateProduct = (product_id, admin_input) => {
	if(admin_input.isAdmin){
	return Product.findByIdAndUpdate({
		name:admin_input.product.name, 
		description:admin_input.product.description,
		price:admin_input.product.price,
		isActive:admin_input.product.isActive
	})
	
	.then((updated_product, error) => {
		if (error){
			return false
		}

		return { 
			message: 'Product has been updated successfully!'
		}
	})

	  let message = Promise.resolve({
	  		message: 'User must be ADMIN to access this'
	  })

		return message.then((value) => {
				return value
		})
}
}


//Archive product controller

module.exports.archiveProduct = (product_id) => {
	return Product.findByIdAndUpdate(product_id, {
		isActive:false
	}).then((archived_course, error) => {
		if (error){
			return false
		}

		return {
			message: 'Product has been archive successfully!'
		}
	})
}