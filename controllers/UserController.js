const User = require('../models/User')
// const Course = require('../models/Product')
const bcrypt = require('bcrypt')
const auth = require('../auth.js')


//register a new user controller

module.exports.register = (data) => { 

let encrypted_password = bcrypt.hashSync(data.password, 10)

let new_user = new User({
	email: data.email,
	password: encrypted_password
})


	return new_user.save().then((created_user, error) =>{
		if(error){
			return false
		}

		return {
			message: 'User successfully registered!'
		}
	})


	
}


//authenticating a user controller 

module.exports.login = (data) => {
	return User.findOne({email: data.email}).then((result) => {
		if (result == null){
			return {
				message: "User doesn't exist!"
			}
		}

		//.compareSync returns a boolean value type
		const is_password_correct = bcrypt.compareSync(data.password, result.password)

		if (is_password_correct) {
			return {
				accessToken: auth.createAccessToken(result)
			}
		}

		return {
			message: 'Password is incorrect!'
		}
	})
}



