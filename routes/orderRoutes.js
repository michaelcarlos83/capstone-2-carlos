const express = require('express')
const router = express.Router()
const ProductController = require('../controllers/ProductController')
const auth = require('../auth')
const OrderController = require('../controllers/OrderController')



//create order non admin

router.post("/new-order", auth.verify, (request, response) => {
	const data = {
	order: request.body,
	isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	OrderController.addOrder(data)
	.then((result) => {
		response.send(result)
	})
})



module.exports = router