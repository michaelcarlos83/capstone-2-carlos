const express = require('express')
const router = express.Router()
const UserController = require('../controllers/UserController')
const auth = require('../auth')


//register a new user route

router.post("/register", (request, response) => {
	UserController.register(request.body)
	.then((result) => {
		response.send(result)
	})
})

//authenticating a user

router.post("/login", (request, response) =>{
	UserController.login(request.body)
	.then((result) => {
		response.send(result)
	})
})

//set user as admin 

router.post("/switch", (request, response) => { 
	UserController.switchToAdmin(request.body)
})


module.exports = router 