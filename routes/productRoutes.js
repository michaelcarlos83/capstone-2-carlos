const express = require('express')
const router = express.Router()
const ProductController = require('../controllers/ProductController')
const auth = require('../auth')



//creating a product admin only

router.post("/newproduct", auth.verify, (request, response) => {
	const data = {
	product: request.body,
	isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	ProductController.addProduct(data)
	.then((result) => {
		response.send(result)
	})
})

//get all active products

router.get('/active', (request, response) => {
	ProductController.getAllActive().then((result) => {
		response.send(result)
	}) 
})

//get single product
router.get("/:productId", (request, response) => {
	ProductController.getProduct(request.params.productId).then((result) => {
		response.send(result)
	})
})

//update Product information Admin only
router.patch("/:productId/update", auth.verify, (request, response) => {
	const admin_input = {
		product:request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	ProductController.addProduct(request.params.productId, admin_input)
	.then((result) => {
		response.send(result)
	})
})


//archive product
router.patch('/:productId/archive', auth.verify, (request, response) => {
 		ProductController.archiveProduct(request.params.ProductId).then((result) => {
 			response.send(result)
 		})
 })


module.exports = router